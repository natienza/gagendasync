#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""main.py: Principal file of the Gcalendar sync script"""

__author__ = "Nicolas ATIENZA"
__credits__ = ["Nicolas ATIENZA"]
__license__ = "GPL"
__version__ = "1.0.1"
__email__ = "nicolas.atienza@nisah.fr"
__status__ = "Development"

import datetime
from time import strptime
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

# Rename the summary of the agenda

DICO = {}
DICO["Convex optimization   A.d'Aspremont"] = "Convex optimization"
DICO["Reinforcement Learning A.Lazaric, M. Pirotta"] = "Reinforcement learn."
DICO["Analyse topologique de données J.Tierny"] = "An. topo Tierny"
DICO["Introduction to statistical learning  N.Vayatis"] = "Intro to stat. learn."
DICO["3D computer vision  P.Monasse, R.Marlet, M.Aubry"] = "3D Comp. Vision"
DICO["Introduction to Medical Image Analysis  h.Delingette, X.Pennec"] = "Intro to Med. Image Analysis"
DICO["Graphs in machine learning M.Valko"] = "Graphs in ML"
DICO["Object recognition and computer vision   J.Sivic, J.Ponce, I.Laptev, C.Schmid"] = "Object recogn. & comp. vision"
DICO["Probabilistic Graphical Models  N.Chopin, P.Latouche"] = "PGM"
DICO["Introduction à l'imagerie numérique Y.Gousseau, J.Delon"] = "Intro à l'imagerie num"
DICO["Foundations of distributed and large scale computing optimization -  E.Chouzenoux"] = "Found. of distri. & large scale comp. opti"
DICO["Sub-pixel image processing  Lionel Moisan"] = "Sub-pixel im. proc."
DICO["Computational statistics  S.Allassonnière"] = "Comp. Stats."
DICO["Image denoising : the human machine competition JM.Morel, G.Facciolo, P.Arias"] = "Image denoising"
DICO["Analyse topologique de données F.Chazal"] = "An. topo Chazal"
DICO["Méthodes mathématiques pour les neurosciences  E.Tanré, R.Veltz"] = "Méthodes maths neuro"
DICO["Deep learning  V.Lepetit"] = "Deep learning"
DICO["Computational optimal transport  G.Peyré"] = "Comp optimal transport"
DICO["Advanced learning for text and graph data ALTEGRAD  M.Vazirgiannis"] = "ALTEGRAD"
DICO["Analyse topologique de données J.Tierny; F.Chazal"] = "An. topo Tierny&Chazal"

def getLocation(location):
    if 'Ulm' in location:
        return 'ENS Ulm'
    elif 'supérieure' in location:
        return 'ENS PS'
    elif 'Descartes' in location or 'DESCARTES' in location:
        return 'Paris Descartes'
    elif 'Centrale' in location:
        return 'Centrale-Supélec'
    elif 'Pierre' in location:
        return 'UPMC Jussieu'
    elif 'Cochin' in location:
        return 'Cochin'
    else:
        return location

def getSummary(summary):
    if summary in DICO.keys():
        return DICO.get(summary)
    return summary

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/calendar.readonly']

def initScreen():
    os.system('touch /tmp/termSize2.txt')
    os.system('stty size > /tmp/termSize2.txt')
    with open('/tmp/termSize2.txt', 'r') as f:
        COLSPAN = int(int(f.read().split(' ')[1])-1)
    if COLSPAN < 0 :
        COLSPAN = 40
    print('#' + '='*(COLSPAN-2) + '#')
    print('#' + ' '*(COLSPAN-2) + '#')
    print('#\033[33m\033[1m{:^{}}\033[0m#'.format('Welcome to your agenda !', COLSPAN-2))
    print('#' + ' '*(COLSPAN-2) + '#')
    print('#' + '='*(COLSPAN-2) + '#')
    print(' ')

def initConnection():
    """
    Initialize the connexion with the GAgenda server and check the credentials
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('cred/token.pickle'):
        with open('cred/token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'cred/credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('cred/token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    return build('calendar', 'v3', credentials=creds)

def getCourse(service):
     # Call the Calendar API
    now = datetime.datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time
    events_result = service.events().list(calendarId='akiooksl1vusav7hf4nvkagsb8@group.calendar.google.com', timeMin=now,
                                          timeMax='2020-12-31T14:17:10.473111Z', singleEvents=True,
                                        orderBy='startTime').execute()
    events = events_result.get('items', [])
    if not events:
        print ('no upcoming events')
        return []
    courses = []
    if os.path.exists('course.txt'):
        with open('course.txt', 'r') as fileCourse:
            courseChosen = fileCourse.read().split('#')
        print('The current course list is : \n')
        for i in range (len(courseChosen)) :
            print ('\033[30m\033[43m{:^2}\033[39m\033[49m\t{}'.format(i+1,courseChosen[i]))
        print('\033[33m\033[1m==> Do you want to add or delete an item from this list ?[y/N]\033[39m')
        print('\033[33m\033[1m--------------------------------------------------------------\033[39m')
        choice = input('\033[33m\033[1m==> \033[39m\033[0m')
        print('')
    if not(os.path.exists('course.txt')) or choice =='y' or choice =='Y':
        for event in events:
            if event['summary'] in courses:
                pass
            else:
                courses.append(event['summary'])
        print ('Classes available in the agenda are : \n')
        index = 1
        deleteBuffer = []
        for course in courses:
            if 'Remise' not in course and 'Réunion' not in course and 'Début' not in course:
                print ('\033[30m\033[43m{:^2}\033[39m\033[49m\t{}'.format(str(index), course))
                index+=1
            else:
                deleteBuffer.append(course)
        for course in deleteBuffer:
            courses.remove(course)
        print('\033[33m\033[1m==> Which class do you take ? [default : all, ex : 1-3-5]\033[39m')
        print('\033[33m\033[1m---------------------------------------------------------\033[39m')
        choice = input ('\033[33m\033[1m==> \033[39m\033[0m').split('-')
        print('')
        if choice[0]== '':
            courseChosen = courses
        else:
            courseChosen =  []
            for i in range (0,len(choice)):
                courseChosen.append(courses[int(choice[i])-1])
        with open('course.txt', 'w') as fileCourse:
            for course in courseChosen:
                if course == courseChosen[-1]:
                    fileCourse.write(course)
                else :
                    fileCourse.write(course + '#')
    relevantEvents = []
    for event in events:
        if event['summary'] in courseChosen:
            relevantEvents.append(event)
    return relevantEvents

def getEventStartDateTime(event):
    return datetime.datetime.strptime(event['start']['dateTime'][0:-6], "%Y-%m-%dT%H:%M:%S")

def getEventEndDateTime(event):
  return datetime.datetime.strptime(event['end']['dateTime'][0:-6], "%Y-%m-%dT%H:%M:%S")

def getEventTime(event, COLSPAN):
    num = int((COLSPAN-15)/2)
    space = (COLSPAN-15)%2
    return '#'*num + ' ' + strHour(getEventStartDateTime(event).hour) + ':' + strHour(getEventStartDateTime(event).minute) + ' - ' + strHour(getEventEndDateTime(event).hour) + ':' + strHour(getEventEndDateTime(event).minute) + ' ' + '#'*num

def strHour(hour):
    if hour == 00:
        return '00'
    else:
        return str(hour)

def printCalendar(events):
    now = datetime.datetime.utcnow()
    weekEvents = [[], [], [], [], []]
    print('Please choose the week you want to print : \n')
    print ('\033[30m\033[43m{:^3}\033[39m\033[49m\tThe current week'.format(0))
    print ('\033[30m\033[43m{:^3}\033[39m\033[49m\tThe next week'.format(1))
    print ('\033[30m\033[43m{:^3}\033[39m\033[49m\tThe week after next week'.format(2))
    print ('\033[30m\033[43m...\033[39m\033[49m\tAnd so on...')
    print ('\033[33m\033[1m==> Which week do you choose ? [default : this week]')
    print ('---------------------------------------------------')
    choice = input('==> \033[39m\033[0m')
    print ('')
    if choice == '':
        weekNum = 0
    else:
        weekNum = int(choice)
    minTime = now
    for event in events:
        testedTime = getEventStartDateTime(event)
        if (testedTime.isocalendar()[1] == now.isocalendar()[1]+weekNum):#if same week
            weekEvents[testedTime.weekday()].append(event)
            if minTime==now or minTime>testedTime:
                minTime = testedTime
    if len(weekEvents[0])==0 and len(weekEvents[1])==0 and len(weekEvents[2])==0 and len(weekEvents[3])==0 and len(weekEvents[4])==0 :
        print('\033[1m No class are planned for this week, take some vacation !\033[0m')
        return
    os.system('touch /tmp/termSize.txt')
    os.system('stty size > /tmp/termSize.txt')
    with open('/tmp/termSize.txt', 'r') as f:
        COLSPAN = int(int(f.read().split(' ')[1])/5 - 2)
    colons = [[], [], [], [], []]
    for dayEvent in weekEvents:
        for event in dayEvent:
            strToprint = '\033[1m{:^{}}\033[0m'.format(getEventTime(event, COLSPAN), COLSPAN)
            colons[getEventStartDateTime(event).weekday()].append(strToprint)
            if len(getSummary(event['summary'])) > COLSPAN:
                strToprint = '\033[1m{:^{}}\033[0m'.format(getSummary(event['summary'])[0:COLSPAN-3] + '...', COLSPAN)
            else:
                strToprint = '\033[1m{:^{}}\033[0m'.format(getSummary(event['summary']), COLSPAN)
            colons[getEventStartDateTime(event).weekday()].append(strToprint)
            if 'location' in event.keys():
                if len(getLocation(event['location']))>COLSPAN:
                    strToprint = '{:{}}'.format(getLocation(event['location'])[0:COLSPAN-3] + '...', COLSPAN)
                else:
                    strToprint = '{:{}}'.format(getLocation(event['location']), COLSPAN)
                    colons[getEventStartDateTime(event).weekday()].append(strToprint)
            if 'description' in event.keys():
                if '\n' in event['description'] :
                    descriptionList = event['description'].split('\n')
                    event['description'] = ''
                    for el in descriptionList:
                        event['description'] += el +' '
                if len(event['description']) > COLSPAN :
                    strToprint = '{:{}}'.format(event['description'][0:COLSPAN-3] + '...', COLSPAN)
                else:
                    strToprint = '{:{}}'.format(event['description'], COLSPAN)
                colons[getEventStartDateTime(event).weekday()].append('\033[2m' + strToprint + '\033[0m')
            else:
                colons[getEventStartDateTime(event).weekday()].append(' '*COLSPAN)
            colons[getEventStartDateTime(event).weekday()].append(' '*COLSPAN)
    print('{:^{}}'.format('Semaine du ' + str(minTime.date()), (COLSPAN +2)*5))
    days = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi']
    print(('\033[1m*' + '-'*COLSPAN + '')*5+'*')
    toBePrint = '|'
    for day in days:
        toBePrint += '{:^{}}'.format(day, COLSPAN) + '|'
    print(toBePrint)
    print( ('*' + '-'*COLSPAN + '')*5+'*\033[0m')
    strToprint = '\033[1m|\033[0m'
    indexes = [0]*5
    Finish  = False
    Finishes = [False]*5
    while not Finish:
        for i in range(len(colons)):
            col = colons[i]
            if len(col)==0:
                col = [' '*COLSPAN]
            if len(col) != indexes[i]:
                strToprint += col[indexes[i]] + '\033[1m|\033[0m'
                indexes[i] += 1
                if indexes[i] == len(col):
                    Finishes[i] = True
                Finish = True
                for test in Finishes:
                    Finish *= test
            else :
                strToprint += ' '*COLSPAN + '\033[1m|\033[0m'
        print(strToprint  )
        strToprint = '\033[1m|\033[0m'
    print(('\033[1m*' + '-'*COLSPAN + '')*5+'*\033[0m')

def main():
    os.system('clear')
    print(' ')
    finish = False
    initScreen()
    service = initConnection()
    relevantEvents = getCourse(service)
    printCalendar(relevantEvents)
    while not finish:
        print('What do you want to do next ? \n')
        print ('\033[30m\033[43m{:^3}\033[39m\033[49m\tChange my classes'.format(0))
        print ('\033[30m\033[43m{:^3}\033[39m\033[49m\tPrint another week'.format(1))
        print ('\033[30m\033[43m{:^3}\033[39m\033[49m\tExit'.format(2))
        print ('\033[33m\033[1m==> What is your choice ? [default : Exit]')
        print ('---------------------------------------------------')
        choice = input('==> \033[39m\033[0m')
        print(' ')
        if choice == '0':
           relevantEvents = getCourse(service)
           printCalendar(relevantEvents)
        elif choice =='1':
            printCalendar(relevantEvents)
        elif choice=='2' or choice=='':
            print('Exiting... Bye !')
            return
        else :
            print('\033[5m\033[31mERROR :\033[0m\033[1m Command not understood, try again\033[0m')

if __name__ == '__main__':
    main()



"""['kind', 'etag', 'id', 'status', 'htmlLink', 'created', 'updated', 'summary', 'description', 'location', 'creator', 'organizer', 'start', 'end', 'recurringEventId', 'originalStartTime', 'iCalUID', 'sequence', 'reminders' """
