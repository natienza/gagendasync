GAgenda Sync
==

Petit utilitaire en ligne de commande qui permet de récupérer les évènements d'un agenda google, de les trier et de les afficher par semaine dans un terminal.

Dépendances
--
La seule dépendance du script est l'API google qui permet de récupérer les données d'un calendrier google.

     pip install google-api-python-client

Utilisation
--
1. Pour installer le script, cloner le dans un espace de travail :

        $ cd /opt
        /opt/$ git clone https://gitlab.com/natienza/gagendasync.git
        /opt/$ cd gagendasync/
    
2. Ensuite, synchroniser le calendrier du master ([ici](https://calendar.google.com/calendar/embed?src=akiooksl1vusav7hf4nvkagsb8%40group.calendar.google.com&ctz=Europe%2FParis )) avec votre compte google et [autoriser le script à accéder à votre calendrier](https://accounts.google.com/ServiceLogin?service=ahsid&passive=true&continue=https://developers.google.com/calendar/quickstart/python)

3. Une fois les autorisations accordées, deux fichiers ont été téléchargés : `credentials.json` et `token.pickle`. Ces fichiers sont à mettre dans un dossier `cred/` dans votre espace de travail. Ce dernier doit alors ressembler à cela :

         gagendasync/
                    main.py
                    README.md
                    cred/
                        credentials.json
                        tocken.pickle

4. Le script est prêt à être éxécuté :

        gagendasync/$ python3 main.py 
        
         
